
This repo contains ReST-by-Examples specifications under examples/
use the various scripts under scripts/ to create API for Android, JavaScript, or iOS.


-To uninstall existing mab:
brew remove https://raw.githubusercontent.com/magnetsystems/installer/master/mob.rb 

-To install latest mab:
brew install https://raw.githubusercontent.com/magnetsystems/installer/master/mob.rb 

-Verify Mobile Generator version:
mob -v

-Start the Mobile Generator:
mob
